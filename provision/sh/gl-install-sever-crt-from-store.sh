#!/bin/bash
#
## Copy secrets file from common directory to gitaly nodes
cp /vagrant/secrets/gitlab-secrets.json /etc/gitlab/

# Copy secrets file from common directory to gitaly nodes
install \
   -D \
   -o gitlab-psql \
   -g gitlab-psql \
   -m 0400 \
   -T /vagrant/secrets/server.crt ~gitlab-psql/.postgresql/root.crt

