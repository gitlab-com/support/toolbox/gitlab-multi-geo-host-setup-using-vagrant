#!/bin/bash
#

echo "Stopping puma, unicorn, sidekiq to prevent changes while configuring secondary"
sudo -i gitlab-ctl stop puma
sudo -i gitlab-ctl stop unicorn
sudo -i gitlab-ctl stop sidekiq

echo "Checking connectivity to Primary DB pot - 5432"
sudo -i gitlab-rake gitlab:tcp_check[10.9.0.31,5432]

##.pgpass in the following format hostname:port:database:username:password
echo "Create a pgpass file"
sudo -i -u gitlab-psql cat <<EOF >/var/opt/gitlab/postgresql/.pgpass
geo1.vagrant:5432:gitlabhq_production:gitlab_replicator:gitlab-rep-pwd
10.9.0.31:5432:gitlabhq_production:gitlab_replicator:gitlab-rep-pwd
EOF
chmod 0600 /var/opt/gitlab/postgresql/.pgpass
chown gitlab-psql:gitlab-psql /var/opt/gitlab/postgresql/.pgpass

echo "Checking access to Primary database gitlabhq_production"
sudo -i -u gitlab-psql /opt/gitlab/embedded/bin/psql --list -U gitlab_replicator -d "dbname=gitlabhq_production" -w -h 10.9.0.31

echo "Adding GEO Secondary config to gitlab.rb"
sudo -i cat /vagrant/config/gl-geo-secondary.rb >> /etc/gitlab/gitlab.rb

echo "Running Reconfigure, sudo gitlab-ctl reconfigure"
sudo -i gitlab-ctl reconfigure

echo "Restart PostgreSQL, sudo gitlab-ctl restart postgresql"
sudo -i gitlab-ctl restart postgresql

echo "Running Reconfigure, sudo gitlab-ctl reconfigure"
sudo -i gitlab-ctl reconfigure

echo "Replicate the Primary!"
sudo -i gitlab-ctl replicate-geo-database --slot-name=geo2_vagrant --no-wait --force --skip-backup --host=10.9.0.31 <<-EOF
gitlab-rep-pwd
EOF


#gitlab-ctl replicate-geo-database \
#   --slot-name=<secondary_node_name> \
#   --host=<primary_node_ip>

##TODO for additional app hosts...  needed ???
##Configure shared secrets. These values can be obtained from the primary GitLab server in /etc/gitlab/gitlab-secrets.json. Copy this file to the secondary servers prior to running the first reconfigure in the steps above.
