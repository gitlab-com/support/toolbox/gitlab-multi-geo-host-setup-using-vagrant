#!/bin/bash
#

sudo cp -p /etc/ssh/sshd_config /etc/ssh/sshd_config.org
cat >> /etc/ssh/sshd_config <<EOF
#
AuthorizedKeysCommand /opt/gitlab/embedded/service/gitlab-shell/bin/gitlab-shell-authorized-keys-check git %u %k
AuthorizedKeysCommandUser git
#
EOF

sudo service ssh reload

sudo cp -p /var/opt/gitlab/.ssh/authorized_keys /var/opt/gitlab/.ssh/authorized_keys.org
sudo rm -f /var/opt/gitlab/.ssh/authorized_keys
