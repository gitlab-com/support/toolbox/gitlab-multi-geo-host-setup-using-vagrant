#!/bin/bash
#

sudo cat /vagrant/config/gl-app-common-settings.rb >> /etc/gitlab/gitlab.rb

#TODO for additional app hosts...
# https://docs.gitlab.com/ee/administration/high_availability/gitlab.html#extra-configuration-for-additional-gitlab-application-servers
#Additional GitLab servers (servers configured after the first GitLab server) need some extra configuration.
#
#Configure shared secrets. These values can be obtained from the primary GitLab server in /etc/gitlab/gitlab-secrets.json. Copy this file to the secondary servers prior to running the first reconfigure in the steps above.
#
#Run touch /etc/gitlab/skip-auto-migrations to prevent database migrations from running on upgrade.
# Only the primary GitLab application server should handle migrations.
