#!/bin/bash
#

# To verify key fingerprint matches, execute the following command on both nodes:
echo 'Output should be the sam eon both Primary and Secondary Geo hosts'
for file in /etc/ssh/ssh_host_*_key; do ssh-keygen -lf $file; done

# You should get an output similar to this one and they should be identical on both nodes:
#  1024 SHA256:FEZX2jQa2bcsd/fn/uxBzxhKdx4Imc4raXrHwsbtP0M root@serverhostname (DSA)
#   256 SHA256:uw98R35Uf+fYEQ/UnJD9Br4NXUFPv7JAUln5uHlgSeY root@serverhostname (ECDSA)
#   256 SHA256:sqOUWcraZQKd89y/QQv/iynPTOGQxcOTIXU/LsoPmnM root@serverhostname (ED25519)
#  2048 SHA256:qwa+rgir2Oy86QI+PZi/QVR+MSmrdrpsuH7YyKknC+s root@serverhostname (RSA)

# Verify that you have the correct public keys for the existing private keys:
# This will print the fingerprint for private keys:
for file in /etc/ssh/ssh_host_*_key; do ssh-keygen -lf $file; done

# This will print the fingerprint for public keys:
for file in /etc/ssh/ssh_host_*_key.pub; do ssh-keygen -lf $file; done

#Note: The output for private keys and public keys command should generate the same fingerprint.
