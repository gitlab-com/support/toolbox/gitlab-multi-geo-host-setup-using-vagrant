#!/bin/bash
# Using https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04
#

echo "Running sudo apt-get -qq update; sudo apt-get -qq install apt-transport-https ca-certificates curl software-properties-common"
sudo apt-get -qq update
sudo apt-get -qq install apt-transport-https ca-certificates curl software-properties-common

echo "Running curl --retry 5 -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -"
curl --retry 5 -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#TODO remove hard coding
echo "Running sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable""
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

echo "Running sudo apt-get -qq update"
sudo apt-get -qq update

#echo "Running apt-cache policy docker-ce"
#apt-cache policy docker-ce

echo "Running sudo apt-get install docker-ce"
sudo apt-get -qq install docker-ce

echo "Running sudo systemctl status docker"
sudo systemctl status docker
