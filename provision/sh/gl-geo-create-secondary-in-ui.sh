#!/bin/bash
#

set -vx

sudo cat >/tmp/geo-create-secondary.rb <<EOF
GeoNode.create(url: "http://geo2.vagrant/", name: "http://geo2.vagrant/", primary: false)
EOF

sudo chmod a+x /tmp/geo-create-secondary.rb

sudo gitlab-rails runner /tmp/geo-create-secondary.rb
