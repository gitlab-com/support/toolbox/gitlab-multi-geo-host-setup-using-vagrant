#!/bin/bash
#

echo "Running sudo gitlab-ctl set-geo-primary-node"
sudo gitlab-ctl set-geo-primary-node

echo "Adding GEO Primary config 01 to gitlab.rb"
sudo cat /vagrant/config/gl-geo-primary-01.rb >> /etc/gitlab/gitlab.rb

echo "Setting replication password for user gitlab_replicator"
echo "Running sudo gitlab-ctl set-replication-password"
sudo gitlab-ctl set-replication-password <<-EOF
gitlab-rep-pwd
gitlab-rep-pwd
EOF

echo "Adding GEO Primary config 02 to gitlab.rb"
sudo cat /vagrant/config/gl-geo-primary-02.rb >> /etc/gitlab/gitlab.rb

echo "Disabling migrations in gitlab.rb"
# Set external_url
sudo sed -i "s,gitlab_rails\['auto_migrate'] = true,gitlab_rails['auto_migrate'] = false,g" /etc/gitlab/gitlab.rb

echo "Running Reconfigure, sudo gitlab-ctl reconfigure"
sudo gitlab-ctl reconfigure

echo "Restart PostgreSQL, sudo gitlab-ctl restart postgresql"
sudo gitlab-ctl restart postgresql

echo "Enabling migrations in gitlab.rb"
sudo sed -i "s,gitlab_rails\['auto_migrate'] = false,gitlab_rails['auto_migrate'] = true,g" /etc/gitlab/gitlab.rb

echo "Running Reconfigure, sudo gitlab-ctl reconfigure"
sudo gitlab-ctl reconfigure

#TODO for additional app hosts...  needed for GEO?
#Configure shared secrets. These values can be obtained from the primary GitLab server in /etc/gitlab/gitlab-secrets.json. Copy this file to the secondary servers prior to running the first reconfigure in the steps above.

