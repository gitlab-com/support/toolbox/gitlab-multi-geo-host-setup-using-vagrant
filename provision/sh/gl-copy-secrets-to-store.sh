#!/bin/bash
#

# Copy secrets file to common directory for later retrieval
cp -f /etc/gitlab/gitlab-secrets.json /vagrant/secrets

# Copy secrets file to common directory for later retrieval
cp -f ~gitlab-psql/data/server.crt /vagrant/secrets

#Copy OpenSSH host keys from the primary node:
cp -f /etc/ssh/ssh_host_*_key* /vagrant/secrets
