#!/bin/bash
#

# Make a backup of any existing SSH host keys:
sudo find /etc/ssh -iname ssh_host_* -exec cp {} {}.backup.`date +%F` \;

# Copy OpenSSH host keys from the primary node:
sudo cp -f /vagrant/secrets/ssh_host_*_key* /etc/ssh/

# Ensure the file permissions are correct:
sudo chown root:root /etc/ssh/ssh_host_*_key*
sudo chmod 0600 /etc/ssh/ssh_host_*_key*

sudo service ssh reload
