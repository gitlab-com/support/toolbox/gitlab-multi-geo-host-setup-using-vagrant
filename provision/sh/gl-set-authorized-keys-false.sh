#!/bin/bash
#

sudo cat >/tmp/set-authorized-keys-fales.rb <<EOF
setting = ApplicationSetting.last
setting.authorized_keys_enabled = false
setting.save!
EOF

sudo chmod a+x /tmp/set-authorized-keys-fales.rb

sudo gitlab-rails runner /tmp/set-authorized-keys-fales.rb
