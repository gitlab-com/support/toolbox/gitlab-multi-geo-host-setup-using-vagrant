#!/bin/bash
#
# $1 gitlab version

GITLAB_VERSION="${1}"

echo "Running sudo apt-get -qq update; sudo apt-get -qq install -y curl openssh-server ca-certificates"
sudo apt-get -qq update
sudo apt-get -qq install curl openssh-server ca-certificates >/dev/null

#TODO Decide on email setup
#sudo apt-get -qq install -y postfix

echo "Running curl -s --retry 5 https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash"
curl -s --retry 5 https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash

#=10.3.8-ee.0
if [[ "${GITLAB_VERSION}" == "" ]]
then
  echo "Running sudo apt-get -qq install gitlab-ee"
  sudo apt-get -qq install gitlab-ee  >/dev/null
else
  echo "Running sudo apt-get -qq install gitlab-ee=${1}"
  sudo apt-get -qq install "gitlab-ee=${1}"  >/dev/null
fi
