#!/bin/bash
#
# $1 - url
# $2 - ip

GITLAB_FQDN="$1"
GITLAB_EXTERNAL_URL="http://$GITLAB_FQDN"
GITLAB_EXTERNAL_IP="$2"

# Shell runner
sudo gitlab-runner register \
  --non-interactive \
  --url "${GITLAB_EXTERNAL_URL}" \
  --registration-token "38hFioub2dMqcz@niduu-sPv" \
  --executor "shell" \
  --description "shell-runner on $(uname -n)" \
  --tag-list "shell" \
  --run-untagged="true" \
  --locked="false"

# Docker runner
sudo gitlab-runner register \
  --non-interactive \
  --url "${GITLAB_EXTERNAL_URL}" \
  --registration-token "38hFioub2dMqcz@niduu-sPv" \
  --docker-extra-hosts "$GITLAB_FQDN:$GITLAB_EXTERNAL_IP" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner on $(uname -n)" \
  --tag-list "docker" \
  --run-untagged="true" \
  --locked="false"

#TODO Docker-in-Docker
#sudo gitlab-runner register \
#  --non-interactive \
#  --url "https://gitlab.com/" \
#  --registration-token "PROJECT_REGISTRATION_TOKEN" \
#  --executor "docker" \
#  --docker-image alpine:latest \
#  --description "docker-runner" \
#  --tag-list "docker,aws" \
#  --run-untagged="true" \
#  --locked="false"


# from docs
#sudo gitlab-runner register \
#  --non-interactive \
#  --url "https://gitlab.com/" \
#  --registration-token "PROJECT_REGISTRATION_TOKEN" \
#  --executor "docker" \
#  --docker-image alpine:latest \
#  --description "docker-runner" \
#  --tag-list "docker,aws" \
#  --run-untagged="true" \
#  --locked="false" \
#  --access-level="not_protected"   ### Not defined, maybe old?
#