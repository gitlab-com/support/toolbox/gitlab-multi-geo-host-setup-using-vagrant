# Unplanned Geo Failover

## Setup
* geo1 Primary v12.2.5
* geo2 Secondary v12.2.5
* Two runners
 
## Summary of actions
1. Initialise setup - should be vagrant up!
1. Add secondary Geo node in UI
1. Login to Secondary node to ensure Oauth has been added
1. Create a project - **Project 100**
1. Add a bash .gitlab-ci.yml to ensure a variety of objects for replication
1. Shown sync'd status
1. Shutdown current primary
1. Promote Secondary to Primary - https://docs.gitlab.com/ee/administration/geo/disaster_recovery/#promoting-a-secondary-node-running-on-a-single-machine
1. Create a project - **Project 999**
1. Add a bash .gitlab-ci.yml to ensure a variety of objects for replication
1. Power up old Primary - https://docs.gitlab.com/ee/administration/geo/disaster_recovery/bring_primary_back.html
1. Reset as a secondary - https://docs.gitlab.com/ee/administration/geo/replication/database.html#step-2-configure-the-secondary-server
1. Shown sync'd status
1. Shutdown current primary
1. Promote Secondary to Primary

Going to jump ahead to Step 7 and capture screenshots and commands from there.

Let's take a snapshot before proceeding to allow us to rollback if needed.

```shell script
vagrant snapshot save 0100-12.2.5-geo1-as-primary
``` 

There are currently two snapshots, each snapshot includes all four VMs.

```shell script
11:52 $ vagrant snapshot list
0100-12.2.5-installed
0110-12.2.5-geo1-as-primary
...
``` 

## Shutdown current primary (**geo1**)
These images show Project 100 and the Pipelines are in sync on geo2.

Geo nodes - geo1 Primary, geo2 Secondary
![Geo nodes - geo1 Primary, geo2 Secondary](geo-nodes-inital-state.png)

geo1 Project 100 Pipelines
![geo1 Project 100 Pipelines](geo1-project-100-pipelines.png)

geo2 Project 100 Pipelines
![geo2 Project 100 Pipelines](geo2-project-100-pipelines.png)

Shutdown geo1

```shell script
vagrant halt geo1
==> geo1: Removing cache buckets symlinks...
==> geo1: Attempting graceful shutdown of VM...
==> geo1: [vagrant-hostsupdater] Removing hosts on suspend disabled
```

## Promote Secondary to Primary 

Using [Promoting a secondary node running on a single machine](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/#promoting-a-secondary-node-running-on-a-single-machine)

Make the suggested changes to `gitlab.rb`
 
```shell script
diff /etc/gitlab/gitlab.rb.secondary /etc/gitlab/gitlab.rb
2202c2202
< roles ['geo_secondary_role']
---
> ###Commented out to force Primary state###roles ['geo_secondary_role']
> roles ['geo_primary_role']   # Needed?  To verify, without this I don't think wal changes are done by Chef
``` 
then run

```shell script

root@geo2:~# gitlab-ctl promote-to-primary-node

---------------------------------------
WARNING: Make sure your primary is down
If you have more than one secondary please see https://docs.gitlab.com/ee/gitlab-geo/disaster-recovery.html#promoting-secondary-geo-replica-in-multi-secondary-configurations
There may be data saved to the primary that was not been replicated to the secondary before the primary went offline. This data should be treated as lost if you proceed.
---------------------------------------

*** Are you sure? (N/y): y

Promoting the PostgreSQL to primary...

could not change directory to "/root": Permission denied
waiting for server to promote.... done
server promoted

Reconfiguring...

Starting Chef Client, version 14.13.11
...
  * file[/var/opt/gitlab/postgresql/data/server.crt] action create (up to date)
  * file[/var/opt/gitlab/postgresql/data/server.key] action create (up to date)
  * template[/var/opt/gitlab/postgresql/data/postgresql.conf] action create
    - update content in file /var/opt/gitlab/postgresql/data/postgresql.conf from 152f0f to 66b88f
    --- /var/opt/gitlab/postgresql/data/postgresql.conf 2019-09-23 08:19:30.790214591 +1000
    +++ /var/opt/gitlab/postgresql/data/.chef-postgresql20190923-32336-1cb405u.conf     2019-09-23 12:31:22.506258408 +1000
    @@ -161,7 +161,7 @@

     # - Settings -

    -wal_level = hot_standby
    +wal_level = minimal
               # (change requires restart)
     #fsync = on       # turns forced synchronization on or off
     #wal_sync_method = fsync    # the default is the first option
    @@ -199,7 +199,7 @@

     # These settings are ignored on a standby server

    -max_wal_senders = 10
    +max_wal_senders = 0
             # (change requires restart)
     #wal_sender_delay = 1s    # walsender cycle time, 1-10000 milliseconds
     #vacuum_defer_cleanup_age = 0 # number of xacts by which cleanup is delayed
    @@ -212,7 +212,7 @@

     # These settings are ignored on a master server

    -hot_standby = on
    +hot_standby = off
               # (change requires restart)
     #wal_receiver_status_interval = 10s # send replies at least this often
               # 0 disables
  * execute[reload postgresql] action run
    - execute /opt/gitlab/bin/gitlab-ctl hup postgresql
  * execute[start postgresql] action run (skipped due to not_if)
  * template[/var/opt/gitlab/postgresql/data/runtime.conf] action create
    - update content in file /var/opt/gitlab/postgresql/data/runtime.conf from 7e8686 to b73206
    --- /var/opt/gitlab/postgresql/data/runtime.conf    2019-09-23 08:21:11.593861615 +1000
    +++ /var/opt/gitlab/postgresql/data/.chef-runtime20190923-32336-kxkfd6.conf 2019-09-23 12:31:23.154261964 +1000
    @@ -26,10 +26,10 @@
     # - Replication
     wal_keep_segments = 10

    -max_standby_archive_delay = 60s # max delay before canceling queries
    +max_standby_archive_delay = 30s # max delay before canceling queries
               # when reading WAL from archive;
               # -1 allows indefinite delay
    -max_standby_streaming_delay = 60s # max delay before canceling queries
    +max_standby_streaming_delay = 30s # max delay before canceling queries
               # when reading streaming WAL;
               # -1 allows indefinite delay

  * execute[reload postgresql] action run
    - execute /opt/gitlab/bin/gitlab-ctl hup postgresql
  * execute[start postgresql] action run (skipped due to not_if)
  * template[/var/opt/gitlab/postgresql/data/pg_hba.conf] action create
    - update content in file /var/opt/gitlab/postgresql/data/pg_hba.conf from 9ed0bf to 3fc9e4
    --- /var/opt/gitlab/postgresql/data/pg_hba.conf     2019-09-23 08:21:12.481849378 +1000
    +++ /var/opt/gitlab/postgresql/data/.chef-pg_hba20190923-32336-1bam8w.conf  2019-09-23 12:31:23.766265372 +1000
    @@ -66,8 +66,6 @@

     # TYPE  DATABASE    USER        CIDR-ADDRESS          METHOD
     # repmgr
    -# fdw
    -host gitlabhq_production gitlab 10.9.0.32/32 md5

     # "local" is for Unix domain socket connections only
     local   all         all                               peer map=gitlab
  * execute[reload postgresql] action run
    - execute /opt/gitlab/bin/gitlab-ctl hup postgresql
  * execute[start postgresql] action run (skipped due to not_if)
  * template[/var/opt/gitlab/postgresql/data/pg_ident.conf] action create (up to date)
Recipe: <Dynamically Defined Resource>
...
...
...
Recipe: <Dynamically Defined Resource>
  * service[unicorn] action restart
    - restart service service[unicorn]
  * service[sidekiq] action restart
    - restart service service[sidekiq]

Running handlers:
Running handlers complete
Chef Client finished, 20/712 resources updated in 26 seconds
gitlab Reconfigured!

Running gitlab-rake geo:set_secondary_as_primary...


You successfully promoted this node!
root@geo2:~#
```

**geo2** is now **Primary** and is in a **standalone** Geo State

geo2 as Primary 
![geo2 as Primary](geo2-is-now-primary.png)

## Create a project - **Project 999**
Created the project, along with the same Bash .gitlab-ci.yml template.

The pipeline show as **Pending, Stuck**.  This is diue to the Runners being registered to geo1, to continue process jobs they now need to be registered against geo2.

```shell script
# sudo gitlab-runner register \
>   --non-interactive \
>   --url "http://geo2.vagrant/" \
>   --registration-token "38hFioub2dMqcz@niduu-sPv" \
>   --executor "shell" \
>   --description "shell-runner on $(uname -n)" \
>   --tag-list "shell" \
>   --run-untagged="true" \
>   --locked="false"
Runtime platform                                    arch=amd64 os=linux pid=21657 revision=a987417a version=12.2.0
Running in system-mode.

Registering runner... succeeded                     runner=38hFioub
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

Project 999 - Pipeline is Pending and stuck
![Project 999 - Pipeline is Pending and stuck](geo2-project-999-pending.png)

Project 999 - successful pipelines after a new runner has been registered
![Project 999 - successful pipelines after a new runner has been registered](geo2-additional-runner-added.png)

## Power up old Primary 
 
See [bring_primary_back.html](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/bring_primary_back.html)

geo1 restarted - show it's current config as Primary with geo2 as Secondary
![geo1 restarted - show it's current config as Primary with geo2 as Secondary](geo1-restarted.png)

geo2 - only shows geo2, no concept of geo1
![geo2 - only shows geo2, no concept of geo1](geo2-primary-with-note.png)

As detailed, the steps are

1. Making the old primary node a secondary node.
1. Promoting a secondary node to a primary node.

### Making the old primary node a secondary node

Follow the detailed steps, you are confirming and configuring the new Primary geo2 to have a secondary as geo1.  Essentially repeating the original setup steps taken when geo1 was configured as Primary.

[Step 1. Configure the primary server](https://docs.gitlab.com/ee/administration/geo/replication/database.html#step-1-configure-the-primary-server)

These steps are not the full list, only highlighted on.

Set the password for the replication user called gitlab_replicator, needs to be done manually.
```shell script
root@geo2:~# gitlab-ctl set-replication-password
Enter password:
Confirm password:
```

Update `/etc/gitlab.rb` to a full Primary with a secondary being allowed access

```shell script
root@geo2:~# diff /etc/gitlab/gitlab.rb.secondary /etc/gitlab/gitlab.rb
...
> postgresql['md5_auth_cidr_addresses'] = ['10.9.0.32/32','10.9.0.31/32']  ## Added in geo1 Ip address
...
< geo_secondary['db_fdw'] = true
---
> #geo_secondary['db_fdw'] = true

...
< gitlab_rails['auto_migrate'] = false
---
> gitlab_rails['auto_migrate'] = true
...
> postgresql['max_replication_slots'] = 2   ## 

# Needs checked!  originally one but now needs to be 2 or we get error 
# ERROR:  replication slots can only be used if wal_level >= replica


```

Complete the `server.crt` exchange.

[Step-2-configure-the-secondary-server](https://docs.gitlab.com/ee/administration/geo/replication/database.html#step-2-configure-the-secondary-server)

```shell script
root@geo1:~# gitlab-ctl stop unicorn
ok: down: unicorn: 1s, normally up
root@geo1:~# gitlab-ctl stop sidekiq
ok: down: sidekiq: 0s, normally up
```

```shell script
root@geo1:~# gitlab-rake gitlab:tcp_check[geo2.vagrant,5432]
TCP connection from 10.9.0.31:33468 to 10.9.0.32:5432 succeeded
```

```shell script
root@geo1:~# install \
>    -D \
>    -o gitlab-psql \
>    -g gitlab-psql \
>    -m 0400 \
>    -T /vagrant/secrets/geo2-server.crt ~gitlab-psql/.postgresql/root.crt
```

```
root@geo1:~# sudo \
>    -u gitlab-psql /opt/gitlab/embedded/bin/psql \
>    --list \
>    -U gitlab_replicator \
>    -d "dbname=gitlabhq_production sslmode=verify-ca" \
>    -W \
>    -h geo2.vagrant
could not change directory to "/root": Permission denied
Password for user gitlab_replicator:
                                             List of databases
        Name         |    Owner    | Encoding |   Collate   |    Ctype    |        Access privileges

---------------------+-------------+----------+-------------+-------------+------------------------------
---
 gitlabhq_production | gitlab      | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 postgres            | gitlab-psql | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 template0           | gitlab-psql | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/"gitlab-psql"
  +
                     |             |          |             |             | "gitlab-psql"=CTc/"gitlab-psq
l"
 template1           | gitlab-psql | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/"gitlab-psql"
  +
                     |             |          |             |             | "gitlab-psql"=CTc/"gitlab-psq
l"
(4 rows)
```

Edit `gitlab.rb` by adding in the Secondary settings to geo1.

```shell script
root@geo1:~# diff /etc/gitlab/gitlab.rb.primary /etc/gitlab/gitlab.rb
< postgresql['md5_auth_cidr_addresses'] = ['10.9.0.31/32','10.9.0.32/32']
---
> postgresql['md5_auth_cidr_addresses'] = ['10.9.0.31/32']   ## Only one IP needed
...
> roles ['geo_secondary_role']     ## now a secondary
>
> geo_secondary['db_fdw'] = true
```

To activate these changes

```shell script
gitlab-ctl reconfigure
```

there will a lot of changes to the configuration files on geo1 as it is reset as a secondary, the Geo tracking database will also be created.

```shell script
gitlab-ctl restart postgresql
gitlab-ctl reconfigure
```

To ensure Postgresql uses the **latest** configuration.

Initiate the replication process

```shell script
root@geo1:~# gitlab-ctl replicate-geo-database --slot-name=geo1_vagrant --host=geo2.vagrant --force

---------------------------------------------------------------
WARNING: Make sure this script is run from the secondary server
---------------------------------------------------------------

*** You are about to delete your local PostgreSQL database, and replicate the primary database. ***
*** The primary geo node is `geo2.vagrant` ***

*** Are you sure you want to continue (replicate/no)? ***
Confirmation: replicate
* Stopping PostgreSQL and all GitLab services
Enter the password for gitlab_replicator@geo2.vagrant:
* Checking for replication slot geo1_vagrant
* Creating replication slot geo1_vagrant
* Backing up postgresql.conf
* Moving old data directory to '/var/opt/gitlab/postgresql/data.1569213382'
* Starting base backup as the replicator user (gitlab_replicator)
pg_basebackup: initiating base backup, waiting for checkpoint to complete
pg_basebackup: checkpoint completed
pg_basebackup: write-ahead log start point: 0/4000028 on timeline 2
pg_basebackup: starting background WAL receiver
50089/50089 kB (100%), 1/1 tablespace
pg_basebackup: write-ahead log end point: 0/40000F8
pg_basebackup: waiting for background process to finish streaming ...
pg_basebackup: base backup completed
* Writing recovery.conf file with sslmode=verify-ca and sslcompression=0
* Restoring postgresql.conf
* Setting ownership permissions in PostgreSQL data directory
* Starting PostgreSQL and all GitLab services
```

In the UI on geo2 add in the new Secondary geo1

geo2 showing as Primary with geo1 as Secondary
![geo2 showing as Primary with geo1 as Secondary](geo2-as-primary-geo1-as-secondary.png)

geo1 showing as Secondary
![geo1 showing as Secondary](geo1-as-secondary.png)

## Shown sync'd status

geo1 is now a secondary and Project 999 has been synchronized.
![geo1 is now a secondary and Project 999 has been synchronized](geo1-secondary-synced.png)


## Shutdown current primary && Promote Secondary to Primary

Repeat as above.





