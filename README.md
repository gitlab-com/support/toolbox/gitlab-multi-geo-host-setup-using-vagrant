# Geo Primary / Secondary Using Vagrant
 
## Provisions
Provisions (default settings)
* Geo Primary
    * `geo1` - 2 CPU, 4096MB
* Geo Secondary
    * `geo2` - 2 CPU, 4096MB
* GitLab Runners x 2
    * `runner109[12]` - 2 CPU, 512MB
    * shared shell, docker executors accepting all jobs

In a **database synchronised** state with runners enabled for use on `geo1`, just needing to add the secondary in the UI!

### Secrets

This directory is being used to store any file containing secrets as needed by the provisioning process
e.g.
```shell script
gitlab-secrets.json
```

from the **geo1** host to allow it to be copied to the other GitLab hosts in our setup.

The file has been added to `.gitignore` so that it's only on your local storage.

> A cut-down version of [vagrant-ha-setup](https://gitlab.com/gl-support/vagrant-ha-setup).

> This is expensive in memory on your local laptop.  Please stop `Docker` and limit the tabs in your browser :-) or maybe you have a 32GB laptop, in which case **go for it!**

See [Setup](#Setup) to install dependencies and provision VMs.

On a MacBook Early 2015 takes around 30 minutes to fully provision.  

Once completed the Geo hosts are configured in the Primary / Secondary roles with the runners registered to `geo1` so we can start creating other objects.

Login to http://geo1.vagrant

Host names are automatically added to your local `/etc/hosts` via the vagrant-hostsupdater plugin.

```text
10.9.0.31 geo1                # GitLab application 1
10.9.0.32 geo2                # GitLab application 2
10.9.0.41 runner10901.vagrant # GitLab gitlab-runner
10.9.0.42 runner10902.vagrant # GitLab gitlab-runner
```

This allows for testing of Geo current features and new updates as they happen.

Wanted to verify questions from customers like
* Geo fail-over
* ...

This creates a **sandpit** for testing operational tasks.

## Install a specific GitLab version
We are using `git tags` [Git-Basics-Tagging](https://git-scm.com/book/en/v2/Git-Basics-Tagging), when a successful setup installation has been completed - we `git tag -a vX.Y.Z` and push that back to the project.

To allow us to provision a specific version while continuing to keep pace with any changes on how this setup should be installed as GitLab iterates.  This will mean that we don't need to add logic in the setup for dealing with different versions.

The scope of this project is to provision a simple base setup to allow further manual configuration for testing.

* Clone the project
* List available tags `git tag`
* Checkout a specific tag `git checkout tag`

To develop on the project, don't use tags and follow the GitLab Flow.

1. Create an issue
1. Create a merge request
1. Checkout the branch
1. `vagrant destroy -f` which will delete any existing VMs and snapshots
1. Update GitLab version and gitlab-runner **version** in `settings-config.yml`

## Operational Tasks

Support issue [Explore Geo Operational Tasks](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1849)

Wanted to verify questions from customers like
* Unplanned Geo Failover? [uplanned-geo-failover](https://gitlab.com/gitlab-com/support/toolbox/gitlab-multi-geo-host-setup-using-vagrant/blob/master/questions-answers/unplanned-geo-failover/uplanned-geo-failover.md)
* ...



## Extracts of the Gitlab `gitlab.rb` Settings Used

geo1 node

```ruby
external_url "http://geo1.vagrant"
gitlab_rails['initial_shared_runners_registration_token'] = "38hFioub2dMqcz@niduu-sPv"
postgresql['sql_user_password'] = '49838209fbb982eb039c744ff63e39e6'
gitlab_rails['db_password'] = 'gitlab-pwd'
roles ['geo_primary_role']
postgresql['listen_address'] = '10.9.0.31'
postgresql['md5_auth_cidr_addresses'] = ['10.9.0.31/32','10.9.0.32/32']
postgresql['max_replication_slots'] = 1
gitlab_rails['auto_migrate'] = true
```

geo2 node

```ruby
external_url "http://geo2.vagrant"
roles ['geo_secondary_role']
postgresql['listen_address'] = '10.9.0.32'
postgresql['md5_auth_cidr_addresses'] = ['10.9.0.32/32']
postgresql['sql_user_password'] = '49838209fbb982eb039c744ff63e39e6'
gitlab_rails['db_password'] = 'gitlab-pwd'
geo_secondary['db_fdw'] = true
gitlab_rails['auto_migrate'] = false
```

## <a name="Setup"></a>Setup

1. Install
* Virtualbox +6.0
* Vagrant +2.2.4
* Vagrant plugins
    * [vagrant-hostsupdater](https://github.com/cogitatio/vagrant-hostsupdater) - adds an entry to your /etc/hosts file on the host system
    * [vagrant-vbguest](https://github.com/dotless-de/vagrant-vbguest) - automatically installs the host's VirtualBox Guest Additions on the guest system
    * [oscar](https://github.com/oscar-stack/oscar)
    
```shell script
vagrant plugin install vagrant-hostsupdater
vagrant plugin install vagrant-vbguest
vagrant plugin install oscar
```

2. Create file `/etc/sudoers.d/vagrant_hostsupdater` with the contents below.  

This will stop the prompting for the root password to allow updates to `/etc/hosts`.

```shell script
sudo vi /etc/sudoers.d/vagrant_hostsupdater
```

* For OSx

Add text

```text
# Allow passwordless startup of Vagrant with vagrant-hostsupdater.
Cmnd_Alias VAGRANT_HOSTS_ADD = /bin/sh -c echo "*" >> /etc/hosts
Cmnd_Alias VAGRANT_HOSTS_REMOVE = /usr/bin/sed -i -e /*/ d /etc/hosts
%admin ALL=(root) NOPASSWD: VAGRANT_HOSTS_ADD, VAGRANT_HOSTS_REMOVE
```

* For Linux

```text
# Allow passwordless startup of Vagrant with vagrant-hostsupdater.
Cmnd_Alias VAGRANT_HOSTS_ADD = /bin/sh -c 'echo "*" >> /etc/hosts'
Cmnd_Alias VAGRANT_HOSTS_REMOVE = /usr/bin/env sed -i -e /*/ d /etc/hosts
%sudo ALL=(root) NOPASSWD: VAGRANT_HOSTS_ADD, VAGRANT_HOSTS_REMOVE 
```

3. Download box - bento/ubuntu-18.04 via 

```shell script
vagrant box add --provider virtualbox "bento/ubuntu-18.04"
```

4. Clone the project

5. Add a license file to enable Geo.

    * **Add your Premium or Ultimate license details to `config/gl-license.txt` (GitLab Premium or higher)**    
    
```shell script
cp <your-license-file> config/gl-license.txt
```    

6. Review settings

Check file `setting-config.yml`

* Timezone - see [complete-timezone-list](https://devanswers.co/ubuntu-change-timezone-synchronize-ntp/#complete-timezone-list)
  - Australia/Brisbane
  - Australia/Melbourne
  - America/Los_Angeles
  - America/New_York
  - America/Los_Angeles
  - Europe/Moscow
  - Europe/Amsterdam  

* Version - leave blank for the latest GitLab, and gitlab-runner packages

7. Initialise Vagrant VMs (20 - 30mins)

```
vagrant up
```

* login to http://geo1.vagrant

Takes about 30 mins on Macbook Pro early 2015

8. Enable secondary Geo host

Go to Admin > Geo > New Node
* name: http://geo2.vagrant/
*  url: http://geo2.vagrant/

Wait 5 minutes then refresh.

9. Login to the secondary!

Login either directly to the Secondary host or via Admin > Geo > Open Projects shown for the Secondary

10. Snapshot install to allow rollback ability! 

```shell script
vagrant snapshot save gitlab-ee-install-11-11-5
==> geo1: Snapshotting the machine as '01-installed-11.11.5'...
==> geo2: Snapshotting the machine as '01-installed-11.11.5'...
==> runner10901: Snapshotting the machine as '01-installed-11.11.5'...
==> runner10902: Snapshotting the machine as '01-installed-11.11.5'...
```

## Common **Vagrant** commands

```
vagrant up                                     # Provisions all of the hosts defined in the Vagranrt file
vagrant up geo1                                # Provisions the VM **geo1**
vagrant snapshot save 01-installed-xx.yy.xx    # Creates a snapshots of all hosts 
vagrant destroy -f geo1                        # Delete the VM **nfs**
vagrant destroy -f                             # Deletes all of the VMs and snapshots
```


### TODO
1. Check out using `gitlab_rails['initial_license_file'] = '/etc/gitlab/company.gitlab-license'` instead of Rails console command.
1. Add standalone Gitaly host.
1. Add geo2 automatically (a rake task is coming!)

