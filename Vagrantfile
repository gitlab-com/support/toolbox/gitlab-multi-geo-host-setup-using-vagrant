# -*- mode: ruby -*-
# vi: set ft=ruby :

#
# Read config from YAML file
#
require 'yaml'

abort("File setting-config.yml is missing") unless File.file?('setting-config.yml')
abort("Copy a valid Premium/Ultimate license file to config/gl-license.txt for Geo to be enabled") unless File.file?('config/gl-license.txt')

settings = YAML.load_file('setting-config.yml')

#TODO Add scripts to run checkpoint commands from documentation

#
# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Manually update the Vagrant boxes - vagrant box update
  config.vbguest.auto_update = false

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  #config.vm.box = "base"
  config.vm.box = "bento/ubuntu-18.04"

  #
  config.hostsupdater.remove_on_suspend = false

  #
  # Multi-host setup
  #


  #
  # Host: geo_node
  #
  settings['roles']['geo_role'].each do |geo_node|
    if geo_node['enabled']
      config.vm.define geo_node['name'] do |node|
        node.vm.boot_timeout = 600
        node.vm.hostname = geo_node['name_fqdn']
        node.vm.network :private_network, :ip => geo_node['ip']
        node.vm.provision :hosts, :sync_hosts => true

        node.vm.provider "virtualbox" do |vb|
          vb.gui    = geo_node['gui']
          vb.cpus   = geo_node['cpus']
          vb.memory = geo_node['memory']
        end

        node.vm.provider "vmware_fusion" do |v|
          v.vmx["memsize"] = geo_node['memory']
          v.vmx["numvcpus"] = geo_node['cpus']
          v.vmx["displayname"] = geo_node['name_fqdn']
        end

        node.vm.provision "shell", name: "provision/sh/bootstrap.sh on vm #{geo_node['name']} Bootstrap", path: "provision/sh/bootstrap.sh", args: settings['setup']['timezone']
        node.vm.provision "shell", name: "provision/sh/timesync-checkpoint.sh on vm #{geo_node['name']} Timesync Checkpoint", path: "provision/sh/timesync-checkpoint.sh"
        node.vm.provision "shell", name: "provision/sh/tools.sh on vm #{geo_node['name']} Tools", path: "provision/sh/tools.sh"
        node.vm.provision "shell", name: "provision/sh/gl-bootstrap.sh on vm #{geo_node['name']} Install GitLab #{settings['setup']['version']}", path: "provision/sh/gl-bootstrap.sh", args: settings['setup']['version']
        node.vm.provision "shell", name: "provision/sh/gl-set-external-url.sh on vm #{geo_node['name']} Set external_url", path: "provision/sh/gl-set-external-url.sh", args: geo_node['external_url']
        node.vm.provision "shell", name: "provision/sh/gl-set-app-common.sh on vm #{geo_node['name']} Configure Application ", path: "provision/sh/gl-set-app-common.sh"
        node.vm.provision "shell", name: "provision/sh/gl-set-authorized-keys.sh on vm #{geo_node['name']} Configure sshd ", path: "provision/sh/gl-set-authorized-keys.sh"

        # PRIMARY or SECONDARY
        if geo_node['state'] == "primary"
          node.vm.provision "shell", name: "provision/sh/gl-set-runner-token.sh on vm #{geo_node['name']} Configure Runner Token", path: "provision/sh/gl-set-runner-token.sh"
          node.vm.provision "shell", name: "provision/sh/gl-reconfigure.sh on vm #{geo_node['name']} Initial Reconfigure", path: "provision/sh/gl-reconfigure.sh"
          node.vm.provision "shell", name: "provision/sh/gl-set-license.sh on vm #{geo_node['name']} Configure Application (Primary)", path: "provision/sh/gl-set-license.sh"
          node.vm.provision "shell", name: "provision/sh/gl-set-geo-primary.sh on vm #{geo_node['name']} Configure Application  (Primary)", path: "provision/sh/gl-set-geo-primary.sh"
          node.vm.provision "shell", name: "provision/sh/gl-postgresql-listening-checkpoint.sh on vm #{geo_node['name']} Postgresql listening?", path: "provision/sh/gl-postgresql-listening-checkpoint.sh"
          node.vm.provision "shell", name: "provision/sh/gl-copy-secrets-to-store.sh on vm #{geo_node['name']} Store Secrets", path: "provision/sh/gl-copy-secrets-to-store.sh"
          node.vm.provision "shell", name: "provision/sh/gl-set-authorized-keys-false.sh on vm #{geo_node['name']} Disable Write to authorised_keys", path: "provision/sh/gl-set-authorized-keys-false.sh"
          # need to run this after geo2 is created - rake task is coming soon!
          # node.vm.provision "shell", name: "provision/sh/gl-geo-create-secondary-in-ui.sh on vm #{geo_node['name']} Create Geo Secondary", path: "provision/sh/gl-geo-create-secondary-in-ui.sh"
        else
          node.vm.provision "shell", name: "provision/sh/gl-copy-secrets-from-store.sh on vm #{geo_node['name']} Install Secrets", path: "provision/sh/gl-copy-secrets-from-store.sh"
          node.vm.provision "shell", name: "provision/sh/gl-reconfigure.sh on vm #{geo_node['name']} Initial Reconfigure", path: "provision/sh/gl-reconfigure.sh"
          node.vm.provision "shell", name: "provision/sh/gl-install-sever-crt-from-store.sh on vm #{geo_node['name']} Install Secrets", path: "provision/sh/gl-install-sever-crt-from-store.sh"
          node.vm.provision "shell", name: "provision/sh/gl-set-geo-secondary.sh on vm #{geo_node['name']} Configure Application  (Secondary)", path: "provision/sh/gl-set-geo-secondary.sh"
          node.vm.provision "shell", name: "provision/sh/gl-copy-ssh-host-keys.sh on vm #{geo_node['name']} Configure SSH keys  (Secondary)", path: "provision/sh/gl-copy-ssh-host-keys.sh"
          node.vm.provision "shell", name: "provision/sh/gl-set-auto-migrate-false.sh on vm #{geo_node['name']} Configure DB migrations to false", path: "provision/sh/gl-set-auto-migrate-false.sh"
          node.vm.provision "shell", name: "provision/sh/gl-reconfigure.sh on vm #{geo_node['name']} GitLab Reconfigure", path: "provision/sh/gl-reconfigure.sh"
          node.vm.provision "shell", name: "provision/sh/gl-restart.sh on vm #{geo_node['name']} GitLab Restart", path: "provision/sh/gl-restart.sh"
        end

        node.vm.provision "shell", name: "provision/sh/gl-ssh-host-keys-checkpoint.sh on vm #{geo_node['name']} SSH Keys Checkpoint", path: "provision/sh/gl-ssh-host-keys-checkpoint.sh"
        node.vm.provision "shell", name: "provision/sh/gl-geo-checkpoint.sh on vm #{geo_node['name']} Geo Checkpoint", path: "provision/sh/gl-geo-checkpoint.sh"
      end
    end
  end

  #
  # Host: runner_node
  #
  settings['roles']['runner_role'].each do |runner_node|
    next unless runner_node['enabled']
    config.vm.define runner_node['name'] do |node|
      node.vm.boot_timeout = 600
      node.vm.hostname = runner_node['name_fqdn']
      node.vm.network :private_network, :ip => runner_node['ip']
      node.vm.provision :hosts, :sync_hosts => true

      node.vm.provider "virtualbox" do |vb|
        vb.gui = runner_node['gui']
        vb.cpus = runner_node['cpus']
        vb.memory = runner_node['memory']
      end

      node.vm.provider "vmware_fusion" do |v|
        v.vmx["memsize"] = runner_node['memory']
        v.vmx["numvcpus"] = runner_node['cpus']
        v.vmx["displayname"] = runner_node['name_fqdn']
      end

      node.vm.provision "shell", name: "provision/sh/bootstrap.sh on vm #{runner_node['name']} Bootstrap", path: "provision/sh/bootstrap.sh", args: settings['setup']['timezone']
      node.vm.provision "shell", name: "provision/sh/timesync-checkpoint.sh on vm #{runner_node['name']} Timesync Checkpoint", path: "provision/sh/timesync-checkpoint.sh"
      node.vm.provision "shell", name: "provision/sh/tools.sh on vm #{runner_node['name']} Tools", path: "provision/sh/tools.sh"
      node.vm.provision "shell", name: "provision/sh/gl-runner-bootstrap.sh on vm #{runner_node['name']} Install GitLab #{settings['setup']['gitlab-runner-version']}", path: "provision/sh/gl-runner-bootstrap.sh", args: settings['setup']['gitlab-runner-version']
      node.vm.provision "shell", name: "provision/sh/gl-runner-docker.sh on vm #{runner_node['name']} Install Docker", path: "provision/sh/gl-runner-docker.sh", args: settings['setup']['external_url']
      node.vm.provision "shell", name: "provision/sh/gl-runner-register.sh on vm #{runner_node['name']} Register Runner", path: "provision/sh/gl-runner-register.sh", args: [settings['setup']['external_url'],settings['setup']['external_ip']]
      node.vm.provision "shell", name: "on vm #{runner_node['name']} Checkpoint", path: "provision/sh/gl-runner-checkpoint.sh", run: "always"
    end
  end

end


#NOT NEEDED   node.vm.provision "shell", name: "provision/sh/gl-postgresql-restart.sh on vm #{geo_node['name']} Restart Postgresql", path: "provision/sh/gl-postgresql-restart.sh"