# GEO Secondary - Minimum setup

# Geo Secondary role
#   configure dependent flags automatically to enable Geo
roles ['geo_secondary_role']

# Secondary address
postgresql['listen_address'] = '10.9.0.32'
postgresql['md5_auth_cidr_addresses'] = ['10.9.0.32/32']

# Database credentials password (defined previously in primary node)
#   replicate same values here as defined in primary node
postgresql['sql_user_password'] = '49838209fbb982eb039c744ff63e39e6'
gitlab_rails['db_password'] = 'gitlab-pwd'

# Enable FDW support for the Geo Tracking Database (improves performance)
geo_secondary['db_fdw'] = true
