
#### Change the initial default admin password and shared runner registration tokens.
####! **Only applicable on initial setup, changing these settings after database
####!   is created and seeded won't yield any change.**
# gitlab_rails['initial_root_password'] = "password"
gitlab_rails['initial_shared_runners_registration_token'] = "38hFioub2dMqcz@niduu-sPv"
