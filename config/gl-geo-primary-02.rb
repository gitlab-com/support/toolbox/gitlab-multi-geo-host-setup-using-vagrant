# Geo Primary role
#   configure dependent flags automatically to enable Geo
roles ['geo_primary_role']

# Primary address
postgresql['listen_address'] = '10.9.0.31'

# Primary and Secondary addresses
postgresql['md5_auth_cidr_addresses'] = ['10.9.0.31/32','10.9.0.32/32']

# Replication settings
#   set this to be the number of Geo secondary nodes you have
postgresql['max_replication_slots'] = 1
# postgresql['max_wal_senders'] = 10
# postgresql['wal_keep_segments'] = 10

#
gitlab_rails['auto_migrate'] = true
